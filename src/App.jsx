import { Tldraw } from 'tldraw'
import './index.css'
import { getAssetUrls } from '@tldraw/assets/selfHosted'

export default function App() {

	const assetUrls = getAssetUrls()
	
	return (
		<div style={{ position: 'fixed', inset: 0 }}>
			<Tldraw assetUrls={assetUrls}/>
		</div>
	)
}